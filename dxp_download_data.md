Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0021-single-shuttle-unit_ssu).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |                  |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0021-single-shuttle-unit_ssu/-/raw/main/02_assembly_drawing/s3-0021-a_single_shuttle_unit_ssu.PDF)               |
| circuit diagram          | [de](https://gitlab.com/ourplant.net/products/s3-0021-single-shuttle-unit_ssu/-/raw/main/03_circuit_diagram/S3-0021-EPLAN-A.pdf)                  |
| maintenance instructions |                  |
| spare parts              |  [de](https://gitlab.com/ourplant.net/products/s3-0021-single-shuttle-unit_ssu/-/raw/main/05_spare_parts/S3-0021-EVL-A.pdf)                 |

